using System;
using System.Collections.Generic;

class Program
{

    static void Solve(int[] arr, int target, out List<HashSet<int>> res, int resLength = 3)
    {
        if (arr.Length < resLength)
        {
            throw new ArgumentException();
        }

        res = new List<HashSet<int>>();
        var state = new int[resLength];
        for (int i = 0; i < resLength; i++)
        {
            state[i] = i;
        }

        while (true)
        {
            state[resLength - 1]++;
            for (int i = resLength - 2; i >= 0; i--)
            {
                int nextInd = i + 1;
                if (state[nextInd] >= (arr.Length - resLength + nextInd + 1))
                {
                    state[i]++;
                }
            }

            if (state[0] == arr.Length - (resLength) + 1)
            {
                break;
            }
            for (int i = 1; i < resLength; i++)
            {
                if (state[i] >= (arr.Length - resLength + i + 1))
                {
                    state[i] = state[i - 1] + 1;
                }
            }

            int sum = 0;
            for (int i = 0; i < resLength; i++)
            {
                sum += arr[state[i]];
            }

            if (sum == target)
            {
                var newSet = new HashSet<int>();
                for (int i = 0; i < resLength; i++)
                {
                    newSet.Add(arr[state[i]]);
                }
                res.Add(newSet);
            }
        }
    }

    static void PrintSolution(List<HashSet<int>> solution)
    {
        foreach (var set in solution)
        {
            foreach (var number in set)
            {
                Console.Write("{0} ", number);
            }
            Console.WriteLine();
        }
    }

    static void Main()
    {
        /**
        Prints:

        0 3 22 
        0 20 5 
        2 3 20
         
        */

        Solve(new int[] { 0, 2, 3, 22, 20, 5 }, 25, out var s1);
        PrintSolution(s1);
    }
}