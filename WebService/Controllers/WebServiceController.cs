﻿using System;
using System.Threading.Tasks;
using System.Text;
using System.Text.Json;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using WebService.Services;
using WebService.Models;


namespace WebService.Controllers
{
    public class WebServiceController
    {
        private SqlFunctionService _sqlFuncService;
        private ConfigurationService _conf;
        public WebServiceController() { }
        public WebServiceController(
            SqlFunctionService sqlFuncService,
            ConfigurationService conf)
        {
            _sqlFuncService = sqlFuncService;
            _conf = conf;
        }
        public Task Process(HttpContext httpContext)
        {
            try
            {
                if (!IsValidQuery(httpContext.Request.Path.Value))
                {
                    return WriteResponse(httpContext, 403);
                }

                var body = ParseBody(httpContext.Request.Body);
                var query = ParsePath(httpContext.Request.Path.Value);
                var meth = ParseMethod(httpContext.Request.Method);

                var result = _sqlFuncService.Invoke(query, meth, body);

                if (result.Code == ErrorCode.ERROR)
                {
                    return WriteResponse(httpContext, 400);
                }
                else if (result.Code == ErrorCode.NOT_FOUND)
                {
                    return WriteResponse(httpContext, 404);
                }

                return WriteResponse(httpContext, 200, JsonSerializer.Serialize(result.Result, new JsonSerializerOptions()));
            }
            catch(Exception)
            {
                return WriteResponse(httpContext, 500);
            }
        }

        bool IsValidQuery(string path)
        {
            return Regex.Match(path, $"/{_conf.Endpoint}/{_conf.Vversion}").Success;
        }

        Task WriteResponse(HttpContext httpContext, int statusCode, string jsonStr = "")
        {
            var buffer = Encoding.UTF8.GetBytes(jsonStr);
            var response = httpContext.Response;
            response.ContentLength = buffer.Length;
            response.ContentType = "application/json";
            response.StatusCode = statusCode;
            return response.Body.WriteAsync(buffer, 0, buffer.Length);
        }

        HttpMethod ParseMethod(string meth)
        {
            if (Enum.TryParse(meth, false, out HttpMethod result))
            {
                return result;
            }
            throw new ArgumentException();
        }

        string ParsePath(string path)
        {
            return path.Replace($"/{_conf.Endpoint}/{_conf.Vversion}", "");
        }

        JsonElement? ParseBody(Stream body)
        {
            using (var sr = new StreamReader(body))
            {
                var argJson = sr.ReadToEnd();
                try
                {
                    return JsonDocument.Parse(argJson).RootElement;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}
