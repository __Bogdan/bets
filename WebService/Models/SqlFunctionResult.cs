using System.Collections.Generic;

namespace WebService.Models
{
    public enum ErrorCode
    {OK = 0, NOT_FOUND, ERROR}
    public class SqlFunctionResult
    {
        public ErrorCode Code;
        public List<Dictionary<string, object>> Result;

        public SqlFunctionResult()
        {
            Result = new List<Dictionary<string, object>>();
        }
    }
}