using System;

namespace WebService.Services
{
    public class ConfigurationService
    {
        public int Port 
        { 
            get 
            {
                if (int.TryParse(Environment.GetEnvironmentVariable("webservice_port"), out var port))
                {
                    return port;
                }
                return 5000;
            }
        }

        public string Endpoint { get => Environment.GetEnvironmentVariable("webservice_endpoint"); }

        public string Host { get => Environment.GetEnvironmentVariable("webservice_host"); }

        public string User { get => Environment.GetEnvironmentVariable("webservice_user"); }

        public string Password { get => Environment.GetEnvironmentVariable("webservice_password"); }

        public string Schema { get => Environment.GetEnvironmentVariable("webservice_schema"); }

        public string Vversion { get; } = "v1";

    }
}