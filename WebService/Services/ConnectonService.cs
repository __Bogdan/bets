using System;
using System.Collections.Generic;
using System.Threading;
using Npgsql;

namespace WebService.Services
{
    public class ConnectionService
    {
        private readonly string _connStr;

        private class ConnectionInfo
        {
            public ConnectionInfo()
            {
                Count = 0;
                Lock = new object();
                Id = DateTime.Now.Millisecond;
            }
            public object Lock { get; }
            public int Count;
            public int Id;
        }

        private Dictionary<NpgsqlConnection, ConnectionInfo> _connectionToInfo;
        private LinkedList<NpgsqlConnection> _connections;

        private object _connServiceLock = new object();

        private LinkedListNode<NpgsqlConnection> _connectionToHand;

        private const int N_CONNS = 2;

        private bool IsConnectionValid(NpgsqlConnection conn)
        {
            return _connectionToInfo.ContainsKey(conn);
        }

        private void InvalidateConnection(NpgsqlConnection conn)
        {
            conn.Dispose();
            Console.WriteLine("Disposed {0}", _connectionToInfo[conn].Id);
            _connectionToInfo.Remove(conn);
        }

        private NpgsqlConnection CreateConnection()
        {
            NpgsqlConnection conn = new NpgsqlConnection(_connStr);
            conn.Open();
            _connectionToInfo[conn] = new ConnectionInfo();
            Console.WriteLine("Created {0}", _connectionToInfo[conn].Id);
            _connections.AddLast(conn);
            return conn;
        }

        public ConnectionService(ConfigurationService conf)
        {
            _connStr =
                $"User ID={conf.User};Password={conf.Password};Host={conf.Host};Port=5432;" +
                "Database=postgres;";
            _connectionToInfo = new Dictionary<NpgsqlConnection, ConnectionInfo>();
            _connections = new LinkedList<NpgsqlConnection>();
            _connectionToHand = null;
        }
        public NpgsqlConnection GetConnection()
        {
            NpgsqlConnection conn = null;
            lock (_connServiceLock)
            {
                if (_connectionToInfo.Keys.Count == N_CONNS)
                {
                    if (_connectionToHand == null) _connectionToHand = _connections.First;
                    while (!IsConnectionValid(_connectionToHand.Value))
                    {
                        var nextConn = _connectionToHand.Next ?? _connections.First;
                        _connections.Remove(_connectionToHand);
                        _connectionToHand = nextConn;
                    }
                    conn = _connectionToHand.Value;
                    _connectionToHand = _connectionToHand.Next ?? _connections.First;

                }
                else if (_connectionToInfo.Keys.Count > N_CONNS)
                {
                    throw new Exception();
                }
                else
                {
                    conn = CreateConnection();
                }

                _connectionToInfo[conn].Count++;
            }
            Monitor.Enter(_connectionToInfo[conn].Lock);
            Console.WriteLine("Acquired {0}", _connectionToInfo[conn].Id);
            return conn;
        }

        public void ReleaseConnection(NpgsqlConnection conn)
        {
            lock (_connServiceLock)
            {
                _connectionToInfo[conn].Count--;
                Monitor.Exit(_connectionToInfo[conn].Lock);
                Console.WriteLine("Released {0}", _connectionToInfo[conn].Id);
                Console.WriteLine(_connectionToInfo[conn].Count);
                if (_connectionToInfo[conn].Count == 0)
                {
                    InvalidateConnection(conn);
                }
            }
        }
    }
}