using System;
using System.Text.Json;
using System.Linq;
using System.Collections.Generic;

using Npgsql;
using NpgsqlTypes;


using WebService.Models;

namespace WebService.Services
{
    public enum HttpMethod
    { GET = 0, POST, PUT, DELETE }

    public class SqlFunctionService
    {
        private ConnectionService _connService;
        private ConfigurationService _conf;

        public SqlFunctionService() { }

        public SqlFunctionService(
            ConnectionService connService,
            ConfigurationService conf)
        {
            _connService = connService;
            _conf = conf;
        }

        public SqlFunctionResult Invoke(string rpc, HttpMethod meth, JsonElement? args)
        {
            var funcNameParts = new List<string>();
            var idArgs = new List<int>();

            var rpcParts = rpc.Split('/', StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < rpcParts.Length; i++)
            {
                if (i % 2 == 0)
                {
                    funcNameParts.Add(rpcParts[i]);
                }
                else
                {
                    idArgs.Add(Convert.ToInt32(rpcParts[i]));
                }
            }

            string funcName = $"{_conf.Schema}.{String.Join('_', funcNameParts)}_{HttpMethodToSqlSuffix(meth)}";


            var argFmtList = new List<string>();
            for (int i = 0; i < idArgs.Count(); i++)
            {
                argFmtList.Add($"@p{i}");
            }
            if (args != null)
            {
                argFmtList.Add("@json");
            }
            string cmdStr = $"select * from {funcName}({String.Join(',', argFmtList)})";

            var conn = _connService.GetConnection();
            var result = new SqlFunctionResult();

            try
            {
                using (var command = new NpgsqlCommand(cmdStr, conn))
                {
                    for (int i = 0; i < idArgs.Count(); i++)
                    {
                        command.Parameters.Add(new NpgsqlParameter($"p{i}", idArgs[i]));
                    }

                    if (args != null)
                    {
                        command.Parameters.Add(new NpgsqlParameter("json", NpgsqlDbType.Json) { Value = args });
                    }

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var entity = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                entity[reader.GetName(i)] = reader[reader.GetName(i)];
                            }
                            result.Result.Add(entity);

                        }
                    }
                    if (result.Result.Count() == 0)
                    {
                        result.Code = ErrorCode.NOT_FOUND;
                    }
                    else
                    {
                        result.Code = ErrorCode.OK;
                    }
                }
            }
            catch (Exception)
            {
                result.Code = ErrorCode.ERROR;
            }
            finally
            {
                _connService.ReleaseConnection(conn);
            }
            return result;
        }

        private static string HttpMethodToSqlSuffix(HttpMethod meth)
        {
            switch (meth)
            {
                case HttpMethod.GET:
                    return "get";
                case HttpMethod.POST:
                    return "ins";
                case HttpMethod.PUT:
                    return "upd";
                case HttpMethod.DELETE:
                    return "del";
            }
            throw new ArgumentException();
        }
    }
}