﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Routing;
using WebService.Services;
using WebService.Controllers;

namespace WebService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRouting();
            services.AddSingleton<ConnectionService>();
            services.AddSingleton<SqlFunctionService>();
            services.AddSingleton<WebServiceController>();
            services.AddSingleton<ConfigurationService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseRouter(routes =>
            {
                routes.MapRoute("{*url}", (httpContext) => 
                {
                    return app.ApplicationServices
                        .GetService<WebServiceController>()
                        .Process(httpContext);
                });
            });
        }

        
    }
}
