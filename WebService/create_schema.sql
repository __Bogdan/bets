drop schema if exists test cascade;
create schema test;

create sequence if not exists test.seq_users;
create sequence if not exists test.seq_comments;

create table if not exists test.users
(
  id int not null default nextval('test.seq_users'::regclass),
  name varchar not null,
  email varchar not null,
  constraint "PK_users" primary key (id),
  constraint "UQ_users_email" unique (email),
  constraint "CHK_users_email" check (email like '%@%')
);
insert into test.users("name", "email") values
  ('bogdan', 'bgdn2014@gmail.com')
;



create table if not exists test.comments
(
  id int not null default nextval('test.seq_comments'::regclass),
  id_user int not null,
  txt varchar not null,
  constraint "PK_comments" primary key (id)
);


create function test.users_get() returns table(id int, name varchar, email varchar) 
  as $$ select * from test.users $$
  language SQL;


create function test.users_ins(args json) returns int 
  as 
  $$  
    insert into test.users("name", "email") values
      (args::json->>'name', args::json->>'email')
    ;
    select 0 
  $$
  language SQL;

create function test.users_comments_ins(user_id int, args json) returns int
  as
  $$
    insert into test.comments("id_user", "txt") values
      (user_id, args::json->>'txt');
    select 0
  $$
  language SQL;


create function test.comments_get(user_id int) returns table(id int, id_user int, txt varchar)
  as $$ select * from test.comments where id = user_id $$
  language SQL;


create function test.comments_get() returns table(id int, id_user int, txt varchar)
  as $$ select * from test.comments $$
  language SQL;

create function test.comments_upd(comment_id int, args json) returns int
  as
  $$
    update test.comments set txt = args::json->>'txt' where id = comment_id;
    select 0
  $$
  language SQL;

create function test.comments_del(comment_id int) returns int
  as
  $$
    delete from test.comments where id = comment_id;
    select 0;
  $$
  language SQL;
