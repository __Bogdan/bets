#/usr/bin/python3
import http.client
import os
import json
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

PORT = os.environ['webservice_port']
ENDPOINT = os.environ['webservice_endpoint']
VVERSION = "v1"

def requestString(path):
    return "/%s/%s%s" % (ENDPOINT, VVERSION, path)

conn = http.client.HTTPSConnection("localhost", PORT)

# Read new user
conn.request("GET", requestString("/users"))
response = conn.getresponse()
assert response.status == 200
respObj = json.loads(response.read().decode('UTF-8'))
assert len(respObj) > 0
newUser = [user for user in respObj if user['name'] == 'bgdn2']
assert newUser[0]['email'] == 'bgdn2@gmail.com'


