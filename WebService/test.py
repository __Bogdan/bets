#!/usr/bin/python3
import http.client
import os
import json
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

PORT = os.environ['webservice_port']
ENDPOINT = os.environ['webservice_endpoint']
VVERSION = "v1"
 
def requestString(path):
    return "/%s/%s%s" % (ENDPOINT, VVERSION, path)

conn = http.client.HTTPSConnection("localhost", PORT)

# Create new user
conn.request("POST", requestString("/users"), '{"name": "bgdn2", "email": "bgdn2@gmail.com"}')
response = conn.getresponse()
print(response.status)
assert response.status == 200
response.read()


# Read new user
conn.request("GET", requestString("/users"))
response = conn.getresponse()
assert response.status == 200
respObj = json.loads(response.read().decode('UTF-8'))
assert len(respObj) > 0
newUser = [user for user in respObj if user['name'] == 'bgdn2']
assert len(newUser) == 1
assert newUser[0]['email'] == 'bgdn2@gmail.com'


# Create new comment
conn.request("POST", requestString("/users/1/comments"), '{"txt": "Testing, 1, 2, 3 ..."}')
response = conn.getresponse()
assert response.status == 200
response.read()


# Read new comment 
conn.request("GET", requestString("/comments"))
response = conn.getresponse()
assert response.status == 200
respObj = json.loads(response.read().decode('UTF-8'))
assert len(respObj) > 0
newUser = [user for user in respObj if user['txt'] == 'Testing, 1, 2, 3 ...']
assert len(newUser) == 1
assert newUser[0]['id_user'] == 1


# Edit comment
conn.request("PUT", requestString("/comments/1"), '{"txt": "New text"}')
response = conn.getresponse()
assert response.status == 200
response.read()


# Read new comment 
conn.request("GET", requestString("/comments/1"))
response = conn.getresponse()
assert response.status == 200
respObj = json.loads(response.read().decode('UTF-8'))
assert len(respObj) == 1 
assert respObj[0]['txt'] == 'New text'


# Delete comment
conn.request("DELETE", requestString("/comments/1"))
response = conn.getresponse()
assert response.status == 200
response.read()


# Ensure comment deleted 
conn.request("GET", requestString("/comments/1"))
response = conn.getresponse()
assert response.status == 404

