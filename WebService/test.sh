#!/bin/bash
set -xe
source env.sh
# For inital launch:
# 
# docker stop some-postgres; docker rm some-postgres
# docker run --name some-postgres -p 5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres
# 
# sleep 10
# 
PGPASSWORD=mysecretpassword psql -h 127.0.0.1 -U postgres < create_schema.sql
python3 $1 test.py && echo "PASSED"
